plugins {
	id("org.jetbrains.kotlin.jvm") version "1.3.41"
	application
	antlr
}

repositories {
	jcenter()
	maven(uri("https://jitpack.io"))
}

dependencies {
	// Use ANTLR4 runtime
	antlr("org.antlr:antlr4:4.7.2")
	// Align versions of all Kotlin components
	implementation(platform("org.jetbrains.kotlin:kotlin-bom"))
	// Use the Kotlin JDK 8 standard library.
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	// Use JUnit5
	testImplementation("org.junit.jupiter:junit-jupiter-api:5.1.0")
	testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.1.0")
}
val projectBasePackage = "frantisekhanzlikbl.kurisu"
val mainClass = "${projectBasePackage}.KurisuKt"
val generatedSourceSetPath = "build/generated_src/java/main"

application {
	this.mainClassName = mainClass
}

java {
	sourceCompatibility = JavaVersion.VERSION_11
	targetCompatibility = JavaVersion.VERSION_11
}

sourceSets {
	main {
		java.srcDir(generatedSourceSetPath)
	}
}

tasks {
	compileKotlin {
		dependsOn(generateGrammarSource)
	}
	generateGrammarSource {
		val generatedSourcePackage = "$projectBasePackage.antlr.generated"
		maxHeapSize = "64m" // Important, throws weird errors about some tokens not recognised in .g4 files without this line
		arguments.addAll(listOf("-package", generatedSourcePackage))
		outputDirectory = File("${generatedSourceSetPath}/${generatedSourcePackage.replace(".", "/")}")
	}

	test {
		useJUnitPlatform()
		systemProperty("junit.jupiter.testinstance.lifecycle.default", "per_class")
	}
}
