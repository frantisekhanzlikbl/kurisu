package frantisekhanzlikbl.kurisu.utils.parser.structure

interface TerminalNode : Node {
	val text: String
}

data class SimpleTerminalNode(override val name: String, override val text: String) : TerminalNode
