package frantisekhanzlikbl.kurisu

import frantisekhanzlikbl.kurisu.antlr.generated.KurisuLexer
import frantisekhanzlikbl.kurisu.antlr.generated.KurisuParser
import frantisekhanzlikbl.kurisu.utilities.string.trimMargin
import frantisekhanzlikbl.kurisu.utils.parser.structure.BranchNode
import frantisekhanzlikbl.kurisu.utils.parser.structure.Node
import frantisekhanzlikbl.kurisu.utils.parser.structure.SimpleBranchNode
import frantisekhanzlikbl.kurisu.utils.parser.structure.SimpleTerminalNode
import frantisekhanzlikbl.kurisu.utils.parser.structure.TerminalNode
import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.CommonTokenStream
import org.antlr.v4.runtime.ListTokenSource
import org.antlr.v4.runtime.Parser
import org.antlr.v4.runtime.ParserRuleContext
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer
import org.antlr.v4.runtime.Token
import org.antlr.v4.runtime.atn.ATNConfigSet
import org.antlr.v4.runtime.dfa.DFA
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.util.BitSet

object NoErrorListener : BaseErrorListener() {
	override fun syntaxError(recognizer: Recognizer<*, *>?, offendingSymbol: Any?, line: Int, charPositionInLine: Int, msg: String?, e: RecognitionException?) {
		Assertions.fail<Unit>("Parser detected syntax error, this is disallowed in unit tests.")
	}

	override fun reportAmbiguity(recognizer: Parser?, dfa: DFA?, startIndex: Int, stopIndex: Int, exact: Boolean, ambigAlts: BitSet?, configs: ATNConfigSet?) {
		Assertions.fail<Unit>("Parser detected ambiguity, this is disallowed in unit tests.")
	}
}

fun List<Token>.toTokenStream() = CommonTokenStream(ListTokenSource(this))
fun getConfiguredParserForTokens(tokens: List<Token>) = KurisuParser(tokens.toTokenStream()).apply {
	addErrorListener(NoErrorListener)
}

fun ParserRuleContext.toBranchNode(): BranchNode = SimpleBranchNode(
	this.javaClass.simpleName,
	this.children.map {
		when (it) {
			is ParserRuleContext -> it.toBranchNode()
			is org.antlr.v4.runtime.tree.TerminalNode -> SimpleTerminalNode(KurisuLexer.VOCABULARY.getSymbolicName(it.symbol.type), it.text)
			else -> throw AssertionError("Unexpected `${it.javaClass.simpleName}` found")
		}
	}
)

fun Node.toStructureString(): String = when (this) {
	is TerminalNode -> this.toStructureString()
	is BranchNode -> this.toStructureString()
	else -> throw AssertionError("Unexpected `${this.javaClass.simpleName}` found")
}

fun TerminalNode.toStructureString() = "${this.name}(${this.text})"
fun BranchNode.toStructureString() = "${this.name}${this.children.joinToString(",", "[", "]", transform = Node::toStructureString)}"

object CommonTokens {
	val eof = TestToken("<EOF>", KurisuLexer.EOF)
}

class ParserTest {
	@Nested inner class Expressions {
		@Nested inner class Operations {
			@Test fun singleOperation() {
				val parsedExpression = getConfiguredParserForTokens(
					listOf(
						TestToken("5", KurisuLexer.LITERAL_NUMBER_INTEGER),
						TestToken("*", KurisuLexer.OPERATOR_MULTIPLICATION),
						TestToken("1.2", KurisuLexer.LITERAL_NUMBER_DECIMAL),
						CommonTokens.eof
					)
				).expression()
				Assertions.assertEquals(
					"""
						|ExpressionOperationBinaryContext[
							|ExpressionNumberLiteralContext[NumberLiteralIntegerContext[LITERAL_NUMBER_INTEGER(5)]],
							|OPERATOR_MULTIPLICATION(*),
							|ExpressionNumberLiteralContext[NumberLiteralDecimalContext[LITERAL_NUMBER_DECIMAL(1.2)]]
						|]
					""".trimMargin(lineSeparator = ""),
					parsedExpression.toBranchNode().toStructureString()
				)
			}

			@Test fun multipleOperations() {
				val parsedExpression = getConfiguredParserForTokens(
					listOf(
						TestToken("5", KurisuLexer.LITERAL_NUMBER_INTEGER),
						TestToken("+", KurisuLexer.OPERATOR_ADDITION),
						TestToken("1.2", KurisuLexer.LITERAL_NUMBER_DECIMAL),
						TestToken("/", KurisuLexer.OPERATOR_DIVISION),
						TestToken("-", KurisuLexer.OPERATOR_SUBTRACTION),
						TestToken("88", KurisuLexer.LITERAL_NUMBER_INTEGER),
						CommonTokens.eof
					)
				).expression()
				Assertions.assertEquals(
					"""
						|ExpressionOperationBinaryContext[
							|ExpressionNumberLiteralContext[NumberLiteralIntegerContext[LITERAL_NUMBER_INTEGER(5)]],
							|OPERATOR_ADDITION(+),
							|ExpressionOperationBinaryContext[
								|ExpressionNumberLiteralContext[NumberLiteralDecimalContext[LITERAL_NUMBER_DECIMAL(1.2)]],
								|OPERATOR_DIVISION(/),
								|ExpressionOperationUnaryContext[
									|OPERATOR_SUBTRACTION(-),
									|ExpressionNumberLiteralContext[NumberLiteralIntegerContext[LITERAL_NUMBER_INTEGER(88)]]
								|]
							|]
						|]
					""".trimMargin(lineSeparator = ""),
					parsedExpression.toBranchNode().toStructureString()
				)
			}
		}
	}
}
