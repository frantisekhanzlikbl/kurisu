package frantisekhanzlikbl.kurisu.utilities.string

/**
 * Detects indent by [marginPrefix] as it does [trimMargin] and replace it with [newIndent].
 *
 * @param marginPrefix non-blank string, which is used as a margin delimiter. Default is `|` (pipe character).
 */
fun String.replaceIndentByMargin(newIndent: String = "", marginPrefix: String = "|", lineSeparator: String = "\n"): String {
	require(marginPrefix.isNotBlank()) { "marginPrefix must be non-blank string." }
	val lines = lines()

	return lines.reindent(length + newIndent.length * lines.size, getIndentFunction(newIndent), { line ->
		val firstNonWhitespaceIndex = line.indexOfFirst { !it.isWhitespace() }

		when {
			firstNonWhitespaceIndex == -1 -> null
			line.startsWith(marginPrefix, firstNonWhitespaceIndex) -> line.substring(firstNonWhitespaceIndex + marginPrefix.length)
			else -> null
		}
	}, lineSeparator)
}

private inline fun List<String>.reindent(
		resultSizeEstimate: Int,
		indentAddFunction: (String) -> String,
		indentCutFunction: (String) -> String?,
		lineSeparator: String = "\n"
): String {
	val lastIndex = lastIndex
	return mapIndexedNotNull { index, value ->
		if ((index == 0 || index == lastIndex) && value.isBlank())
			null
		else
			indentCutFunction(value)?.let(indentAddFunction) ?: value
	}
		.joinTo(StringBuilder(resultSizeEstimate), lineSeparator)
		.toString()
}

private fun getIndentFunction(indent: String) = when {
	indent.isEmpty() -> { line: String -> line }
	else -> { line: String -> indent + line }
}

/**
 * Trims leading whitespace characters followed by [marginPrefix] from every line of a source string and removes
 * the first and the last lines if they are blank (notice difference blank vs empty).
 *
 * Doesn't affect a line if it doesn't contain [marginPrefix] except the first and the last blank lines.
 *
 * Doesn't preserve the original line endings.
 *
 * @param marginPrefix non-blank string, which is used as a margin delimiter. Default is `|` (pipe character).
 * @param lineSeparator line ending to use. Default is `\n` (newline).
 * @see trimIndent
 * @see kotlin.text.isWhitespace
 */
fun String.trimMargin(marginPrefix: String = "|", lineSeparator: String = "\n"): String =
		replaceIndentByMargin("", marginPrefix, lineSeparator)
