package frantisekhanzlikbl.kurisu.exception

object InterpreterExceptions {
	class UnexpectedNodeException(message: String?) : Exception(message)
	class InvalidElementOnStackException(message: String?) : Exception(message)
}
