package frantisekhanzlikbl.kurisu

import frantisekhanzlikbl.kurisu.antlr.generated.KurisuLexer
import frantisekhanzlikbl.kurisu.antlr.generated.KurisuParser
import frantisekhanzlikbl.kurisu.ast.toAstNode
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream

fun main() {
	val lexer = KurisuLexer(
		CharStreams.fromString(
			"""
				|5 + (-1.5 * 3)
				|12 / -(4 - 1)
			""".trimMargin()
		)
	)
	val parser = KurisuParser(CommonTokenStream(lexer))
	val interpreter = FileInterpreter()
	interpreter.interpret(parser.file().toAstNode())
	println(interpreter.stack)
}
