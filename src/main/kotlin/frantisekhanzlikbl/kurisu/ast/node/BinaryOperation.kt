package frantisekhanzlikbl.kurisu.ast.node

interface BinaryOperation : Expression {
	data class Addition(override val left: Expression, override val right: Expression) : BinaryOperation
	data class Subtraction(override val left: Expression, override val right: Expression) : BinaryOperation
	data class Multiplication(override val left: Expression, override val right: Expression) : BinaryOperation
	data class Division(override val left: Expression, override val right: Expression) : BinaryOperation

	val left: Expression
	val right: Expression
}
