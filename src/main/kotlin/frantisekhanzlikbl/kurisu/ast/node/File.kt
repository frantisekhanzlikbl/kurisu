package frantisekhanzlikbl.kurisu.ast.node

import frantisekhanzlikbl.kurisu.generic.ast.Node

data class File(val expressions: Collection<Expression>) : Node
