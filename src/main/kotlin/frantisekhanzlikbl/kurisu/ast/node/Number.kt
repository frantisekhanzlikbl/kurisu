package frantisekhanzlikbl.kurisu.ast.node

interface Number : Literal {
	data class Integer(val value: Int) : Number
	data class Decimal(val value: Float) : Number
}
