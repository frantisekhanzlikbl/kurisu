package frantisekhanzlikbl.kurisu.ast.node

import frantisekhanzlikbl.kurisu.generic.ast.Node

interface Expression : Node
