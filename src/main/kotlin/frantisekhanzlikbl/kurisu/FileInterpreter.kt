package frantisekhanzlikbl.kurisu

import frantisekhanzlikbl.kurisu.ast.node.BinaryOperation
import frantisekhanzlikbl.kurisu.ast.node.Expression
import frantisekhanzlikbl.kurisu.ast.node.File
import frantisekhanzlikbl.kurisu.ast.node.Literal
import frantisekhanzlikbl.kurisu.ast.node.Number
import frantisekhanzlikbl.kurisu.ast.node.UnaryOperation
import frantisekhanzlikbl.kurisu.exception.InterpreterExceptions
import frantisekhanzlikbl.kurisu.generic.Interpreter
import java.util.Stack

class FileInterpreter : Interpreter<File> {
	private fun reportUnexpectedNode(nodeType: String): Nothing = throw InterpreterExceptions.UnexpectedNodeException("Approached unexpected node: `${nodeType}`")
	private fun reportInvalidElementOnStack(elementType: String): Nothing = throw InterpreterExceptions.InvalidElementOnStackException("Found invalid element on stack: `${elementType}`")
	private inline fun <reified TargetType> Any.attemptCast() = this as? TargetType ?: reportInvalidElementOnStack(this.javaClass.simpleName)
	val stack = Stack<Any>()
	override fun interpret(node: File) {
		node.expressions.forEach {
			this.interpret(it)
		}
	}

	private fun interpret(node: Expression) = when (node) {
		is Literal -> this.interpret(node)
		is UnaryOperation -> this.interpret(node)
		is BinaryOperation -> this.interpret(node)
		else -> reportUnexpectedNode(node.javaClass.simpleName)
	}

	private fun interpret(node: UnaryOperation) = when (node) {
		is UnaryOperation.Minus -> this.interpret(node)
		else -> reportUnexpectedNode(node.javaClass.simpleName)
	}

	private fun interpret(node: BinaryOperation) {
		this.interpret(node.left)
		this.interpret(node.right)
		// The numbers have to be popped-off the stack in reverse order in which they were pushed onto it
		val operand1 = this.stack.pop().attemptCast<kotlin.Number>().toDouble()
		val operand0 = this.stack.pop().attemptCast<kotlin.Number>().toDouble()
		this.stack += when (node) {
			is BinaryOperation.Addition -> operand0 + operand1
			is BinaryOperation.Subtraction -> operand0 - operand1
			is BinaryOperation.Multiplication -> operand0 * operand1
			is BinaryOperation.Division -> operand0 / operand1
			else -> reportUnexpectedNode(node.javaClass.simpleName)
		}
	}

	private fun interpret(node: Literal) = when (node) {
		is Number -> this.interpret(node)
		else -> reportUnexpectedNode(node.javaClass.simpleName)
	}

	private fun interpret(node: Number) = when (node) {
		is Number.Integer -> this.interpret(node)
		is Number.Decimal -> this.interpret(node)
		else -> reportUnexpectedNode(node.javaClass.simpleName)
	}

	private fun interpret(node: Number.Integer) {
		this.stack += node.value
	}

	private fun interpret(node: Number.Decimal) {
		this.stack += node.value
	}

	private fun interpret(node: UnaryOperation.Minus) {
		this.interpret(node.operand)
		this.stack += -this.stack.pop().attemptCast<kotlin.Number>().toDouble()
	}
}
